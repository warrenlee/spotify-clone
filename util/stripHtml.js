export const stripHtml = (str) => {
  return str.replace(/(<([^>]+)>)/gi, '')
}
