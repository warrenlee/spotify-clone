import {
  SPOTIFY_AUTHORIZED_ENDPOINT,
  SPOTIFY_USERINFO_ENDPOINT,
  SPOTIFY_TOKEN_ENDPOINT,
  SPOTIFY_SCOPE,
} from './constants'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Spotify Clone Web App',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [{ src: 'https://sdk.scdn.co/spotify-player.js', ssr: false }],
  },

  target: 'static',

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/css/main'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['~/plugins/v-dropdown-menu.js', '~/plugins/mdi-vue.js'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/stylelint-module',
    'nuxt-windicss',
    '@nuxtjs/composition-api/module',
    '@nuxtjs/pwa',
    '@nuxtjs/google-fonts',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ['mdi-vue'],
  },

  router: {
    middleware: ['auth'],
  },

  auth: {
    strategies: {
      social: {
        scheme: 'oauth2',
        endpoints: {
          authorization: SPOTIFY_AUTHORIZED_ENDPOINT,
          token: SPOTIFY_TOKEN_ENDPOINT,
          userInfo: SPOTIFY_USERINFO_ENDPOINT,
        },
        clientId: process.env.SPOTIFY_CLIENT_ID,
        scope: SPOTIFY_SCOPE,
        responseType: 'code',
        grantType: 'authorization_code',
        codeChallengeMethod: 'S256',
      },
    },
    redirect: {
      login: '/',
      logout: '/',
      callback: '/callback',
      home: '/',
    },
  },

  googleFonts: {
    families: {
      Almarai: [400, 700],
    },
  },
}
