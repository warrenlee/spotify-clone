import api from '~/services/api'

export const state = () => ({
  playerState: {},
  deviceId: '',
  uri: '',
})

export const mutations = {
  SET_PLAYER_STATE(state, payload) {
    state.playerState = payload
  },
  SET_URI(state, uri) {
    state.uri = uri
  },
  SET_DEVICE_ID(state, id) {
    state.deviceId = id
  },
}

export const actions = {
  // async getPlayerState({ commit }, $auth) {
  //   if ($auth.loggedIn) {
  //     const response = await axios
  //       .get('/me/player', {
  //         headers: {
  //           Authorization: $auth.strategy.token.get(),
  //         },
  //       })
  //       .then(({ data }) => data)

  //     if (response) {
  //       commit('SET_PLAYER_STATE', response)
  //     }
  //   }
  // },
  playSong({ state, commit }, { uri, $auth }) {
    if (state.deviceId) {
      api('/me/player/play', $auth, {
        method: 'PUT',
        data: {
          uris: [uri],
        },
        params: {
          device_id: state.deviceId,
        },
      }).then(() => {
        commit('SET_URI', uri)
      })
    }
  },
  playArtist({ dispatch }, { uri, $auth }) {
    dispatch('playContext', { uri, $auth })
  },
  playPlaylist({ dispatch }, { uri, $auth }) {
    dispatch('playContext', { uri, $auth })
  },
  playContext({ state, dispatch, commit }, { uri, $auth }) {
    if (state.deviceId) {
      api('/me/player/play', $auth, {
        method: 'PUT',
        data: {
          context_uri: uri,
        },
        params: {
          device_id: state.deviceId,
        },
      }).then(() => {
        commit('SET_URI', uri)
        // dispatch('getPlayerState', $auth)
      })
    }
  },
}
