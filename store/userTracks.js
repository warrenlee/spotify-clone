import api from '~/services/api'

export const state = () => ({
  recentlyPlayedTracks: [],
  topTrackItems: [],
  topArtistItems: [],
  userPlaylists: [],
})

export const mutations = {
  ADD_RECENTLY_PLAYED_ITEMS(state, items) {
    state.recentlyPlayedTracks = items
  },
  ADD_TOP_TRACK_ITEMS(state, items) {
    state.topTrackItems = items
  },
  ADD_TOP_ARTIST_ITEMS(state, items) {
    state.topArtistItems = items
  },
  ADD_USER_PLAYLISTS(state, items) {
    state.userPlaylists = items
  },
}

export const actions = {
  async getUserRecentlyPlayedTracks({ commit }, $auth) {
    if ($auth.loggedIn) {
      const response = await api('/me/player/recently-played', $auth, {
        params: {
          limit: 5,
        },
      })

      if (response.items) {
        commit(
          'ADD_RECENTLY_PLAYED_ITEMS',
          response.items.map((item) => item.track)
        )
      }
    }
  },
  async getUserTopTracks({ commit }, $auth) {
    if ($auth.loggedIn) {
      const response = await api('/me/top/tracks', $auth, {
        params: {
          limit: 5,
        },
      })

      if (response.items) {
        commit('ADD_TOP_TRACK_ITEMS', response.items)
      }
    }
  },
  async getUserTopArtists({ commit }, $auth) {
    if ($auth.loggedIn) {
      const response = await api('/me/top/artists', $auth, {
        params: {
          limit: 5,
        },
      })

      if (response.items) {
        commit('ADD_TOP_ARTIST_ITEMS', response.items)
      }
    }
  },
  async getUserPlaylists({ commit }, $auth) {
    if ($auth.loggedIn) {
      const response = await api('/me/playlists', $auth, {
        params: {
          limit: 20,
        },
      })

      if (response.items) {
        commit(
          'ADD_USER_PLAYLISTS',
          response.items.filter((item) => item.name)
        )
      }
    }
  },
}
