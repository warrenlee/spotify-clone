export default {
  actions: {
    async nuxtServerInit({ dispatch }, { $auth }) {
      if ($auth.loggedIn) {
        await dispatch('userTracks/getUserRecentlyPlayedTracks', $auth)
        await dispatch('userTracks/getUserTopTracks', $auth)
        await dispatch('userTracks/getUserTopArtists', $auth)
        await dispatch('userTracks/getUserPlaylists', $auth)
      }
    },
  },
}
