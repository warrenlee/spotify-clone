import axios from '~/services/axios'

const api = (url, $auth, config = {}) => {
  return axios
    .request({
      url,
      ...config,
      headers: {
        Authorization: $auth.strategy.token.get(),
      },
    })
    .then(({ data }) => data)
}

export default api
