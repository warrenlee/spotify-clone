import axios from 'axios'
import { SPOTIFY_API_ENDPOINT } from '~/constants'

const instance = axios.create({
  baseURL: SPOTIFY_API_ENDPOINT,
  headers: {
    'Content-Type': 'application/json',
  },
})

export default instance
