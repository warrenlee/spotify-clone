export default {
  theme: {
    extend: {
      fontFamily: {
        almarai: ['Almarai', 'sans-serif'],
      },
      gridTemplateColumns: {
        layout: '240px minmax(0, 1fr)',
        trackGrid: 'repeat(auto-fill, minmax(180px, 1fr))',
      },
    },
  },
  /**
   * Write windi classes in html attributes.
   * @see https://windicss.org/features/attributify.html
   */
  attributify: true,
  plugins: [
    require('windicss/plugin/aspect-ratio'),
    require('windicss/plugin/line-clamp'),
  ],
}
