import { useStore, computed } from '@nuxtjs/composition-api'

const useUser = () => {
  const store = useStore()

  return {
    profileImage: computed(() => store.state.auth.user.images[0].url),
    displayName: computed(() => store.state.auth.user.display_name),
    followers: computed(() => store.state.auth.user.followers.total),
    userType: computed(() => store.state.auth.user.product),
  }
}

export default useUser
