import { useStore, computed, useContext } from '@nuxtjs/composition-api'
import { get, isEmpty } from 'lodash'
import api from '~/services/api'

const repeatModeEnum = ['off', 'context', 'track']

const useMusicPlayer = () => {
  const store = useStore()
  const { $auth } = useContext()

  const resume = () => {
    api('/me/player/play', $auth, { method: 'PUT' })
  }

  const paused = computed(() =>
    get(store.state.musicPlayer.playerState, 'paused', true)
  )

  const pause = () => {
    api('/me/player/pause', $auth, { method: 'PUT' })
  }

  const shuffleState = computed(() =>
    get(store.state.musicPlayer.playerState, 'shuffle', false)
  )

  const repeatMode = computed(() =>
    get(store.state.musicPlayer.playerState, 'repeat_mode', 0)
  )

  const toggleShuffle = () => {
    api('/me/player/shuffle', $auth, {
      method: 'PUT',
      params: { state: !shuffleState.value },
    })
  }

  const toggleRepeatMode = () => {
    const index = (repeatMode.value + 1) % repeatModeEnum.length
    const state = repeatModeEnum[index] || 'off'

    api('/me/player/repeat', $auth, {
      method: 'PUT',
      params: { state },
    })
  }

  const togglePlay = () => {
    if (paused.value) {
      resume()
    } else {
      pause()
    }
  }

  return {
    resume,
    pause,
    togglePlay,
    isActive: computed(() => !isEmpty(store.state.musicPlayer.playerState)),
    deviceId: computed(() => get(store.state.musicPlayer, 'deviceId')),
    uri: computed(() =>
      get(store.state.musicPlayer.playerState, 'track_window.current_track.uri')
    ),
    cover: computed(() =>
      get(
        store.state.musicPlayer.playerState,
        'track_window.current_track.album.images[0].url'
      )
    ),
    trackName: computed(() =>
      get(
        store.state.musicPlayer.playerState,
        'track_window.current_track.name'
      )
    ),
    position: computed(() =>
      get(store.state.musicPlayer.playerState, 'position', 0)
    ),
    duration: computed(() =>
      get(store.state.musicPlayer.playerState, 'duration', 0)
    ),
    paused,
    repeatMode,
    shuffleState,
    toggleShuffle,
    toggleRepeatMode,
    trackArtists: computed(() => {
      const items = get(
        store.state.musicPlayer.playerState,
        'track_window.current_track.artists',
        []
      )
      const artists = items
        .slice(0, 3)
        .map(({ name }) => name)
        .join(', ')

      return items.length > 3 ? `${artists} and more` : artists
    }),
  }
}

export default useMusicPlayer
