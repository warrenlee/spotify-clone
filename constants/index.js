export const SPOTIFY_AUTHORIZED_ENDPOINT =
  'https://accounts.spotify.com/authorize'

export const SPOTIFY_TOKEN_ENDPOINT = 'https://accounts.spotify.com/api/token'

export const SPOTIFY_USERINFO_ENDPOINT = 'https://api.spotify.com/v1/me'

export const SPOTIFY_API_ENDPOINT = 'https://api.spotify.com/v1'

export const SPOTIFY_SCOPE = [
  'user-read-private',
  'user-read-email',
  'streaming',
  'user-read-playback-state',
  'user-read-playback-position',
  'user-read-recently-played',
  'user-modify-playback-state',
  'user-read-currently-playing',
  'playlist-read-private',
  'user-library-read',
  'user-top-read',
  'user-follow-read',
]
